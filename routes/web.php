<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 Route::get('/listuser','UsersController@index');
Route::get('/insert','UsersController@insert');
Route::get('/update','UsersController@update');
  Route::get('/listuser', 'UsersController@view');
 Route::get('/dis', function () {
    return view('display');
});
Route::get('get-form', 'HandleController@getForm')->name('get_form');//'HandleController@compare'
Route::post('compare', 'HandleController@compare')->name('compare');

Route::post('/check', function () {
    return view('check');
});
Route::post('comparedata', 'HandleController@comparedata');
Route::post('handle-form', 'HandleController@handleRequest');
Route::get('get','UsersController@setcon');
Route::get('home', function () {
    $value = session('key');
    session(['key' => $database_01]);
    session(['key' => $database_02]);
});    

