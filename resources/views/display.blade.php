<?php
	$connect = mysqli_connect('localhost','root','','test1');
	mysqli_set_charset($connect,"utf8");
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Form DB</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

    <style>
    .form-control {
        border-radius: 11px;
        font-size: 16px;
        border: 1px solid #337ab7;
    }
    .h3, h3{
        
        font-weight:bold;
        font-size: 30px;
    }
    .h2, h2{
        color: #E32B39;
        font-weight:bold;
    }
    
    .groub-form .form-group{
        margin-right: 0 !important;
        margin-left: 0 !important;
        font-size: 18px;
    }
    .btn-primary {
        font-size: 20px;
    }
    .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
        padding: 4px;
    }
    .btn-primary{
        padding: 1px 3px;
    }
    </style>
<body>

<div class="jumbotron text-center">
  <h3 <?php echo 'style="color: #00A3C7"';?>>CONNECT DATABASE</h3>
</div>
  
<div class="container">
  <div class="row">
  @php 
    $db = Session::get("db01");
    $db2 = Session::get("db02");
  @endphp
     @if(isset($db) && (isset($db2)))
        <div style="text-align:right">
        <a class="btn btn-primary" href="<?php echo  route('get_form', ['history' => 1]) ?>">Xem lịch sử so sánh</a>
            
        </div>
        <form  action="{{ url('compare') }}" method="POST">
        {{ csrf_field()}}
            <div class="col-sm-6">
            
                <h2 <?php echo 'style="color:#E32B39"';?>>Database 1</h2>
                <div class="form-horizontal groub-form">
                    <div class="form-group">
                        <label for="hostname">Hostname:</label>
                        <input type="text" class="form-control" id="hostname" value="{{$db['host']}}" name="database[hostname]"/>
                    </div>
                    
                    <div class="form-group">
                        <label for="port">Port:</label>
                        <input type="text" class="form-control" id="port" value="{{$db['port']}}"  name="database[port]">
                    </div>
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" id="username" value="{{$db['username']}}"  name="database[username]"/>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password" value="{{$db['password']}}" name="database[password]"/>
                    </div>
                    <div class="form-group">
                        <label for="dbname">DB name:</label>
                        <input type="text" class="form-control" id="dbname" value="{{$db['dbname']}}" name="database[dbname]"/>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h2 <?php echo 'style="color: #3ad8e0"';?>>Database 2</h2>
                <div class="form-horizontal groub-form">                   
                    <div class="form-group">
                        <label for="hostname">Hostname:</label>
                        <input type="text" class="form-control" id="hostname2" value="{{$db2['host']}}" name="database2[hostname2]"/>
                    </div>
                
                    <div class="form-group">
                        <label for="port">Port:</label>
                        <input type="text" class="form-control" id="port2" value="{{$db2['port']}}" name="database2[port2]"/>
                    </div>
                    <div class="form-group">
                        <label for="username">Username:</label>
                        <input type="text" class="form-control" id="username2" value="{{$db2['username']}}"  name="database2[username2]"/>
                    </div>
                    <div class="form-group">
                        <label for="password">Password:</label>
                        <input type="password" class="form-control" id="password2" value="{{$db2['password']}}"  name="database2[password2]"/>
                    </div>
                    <div class="form-group">
                        <label for="dbname">DB name:</label>
                        <input type="text" class="form-control" id="dbname2" value="{{$db2['dbname']}}" name="database2[dbname2]"/>
                    </div>
                </div>
            </div>
            <div style="text-align:center">
                    <button type="submit" class="btn btn-primary" id="compare"> Cấu trúc DB</button> 
            </div>
        </form> <br/>
    @else
    Chưa có session
    <div style="text-align:right">
        <a class="btn btn-primary" href="<?php echo  route('get_form', ['history' => 1]) ?>">Xem lịch sử so sánh</a>
    </div>
    <form  action="{{ url('compare') }}" method="POST">
            {{ csrf_field()}}
            
                <div class="col-sm-6">
                    <h3 <?php echo 'style="color:#E32B39"';?>>Database 1</h3>
                    <div class="form-horizontal groub-form">                      
                        <div class="form-group">
                            <label for="hostname">Hostname:</label>
                            <input type="text" class="form-control" id="hostname"  name="database[hostname]"/>
                        </div>
                        
                        <div class="form-group">
                            <label for="port">Port:</label>
                            <input type="text" class="form-control" id="port"  name="database[port]">
                        </div>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username"  name="database[username]"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password"  name="database[password]"/>
                        </div>
                        <div class="form-group">
                            <label for="dbname">DB name:</label>
                            <input type="text" class="form-control" id="dbname"  name="database[dbname]"/>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <h3 <?php echo 'style="color: #4267b2"';?>>Database 2</h3>
                    <div class="form-horizontal groub-form">                     
                        <div class="form-group">
                            <label for="hostname">Hostname:</label>
                            <input type="text" class="form-control" id="hostname2"  name="database2[hostname2]"/>
                        </div>
                    
                        <div class="form-group">
                            <label for="port">Port:</label>
                            <input type="text" class="form-control" id="port2"  name="database2[port2]"/>
                        </div>
                        <div class="form-group">
                            <label for="username">Username:</label>
                            <input type="text" class="form-control" id="username2"  name="database2[username2]"/>
                        </div>
                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" id="password2"  name="database2[password2]"/>
                        </div>
                        <div class="form-group">
                            <label for="dbname">DB name:</label>
                            <input type="text" class="form-control" id="dbname2"  name="database2[dbname2]"/>
                        </div>
                    </div>
                </div>
                <div style="text-align:center">
                    <button type="submit" class="btn btn-primary" id="compare"> Cấu trúc DB</button><br>
                </div>
                
            </form> <br />
    @endif         
   </div>
   <div class="col-sm-12">
   <div class="table table-bordered">
    @if(!empty($structure) )
        @if($check==1)
            <table class="table">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>hostname</th>
                        <th>port</th>
                        <th>username</th>
                        <th>password</th>
                        <th>dbname</th>
                        <th>hostname2</th>
                        <th>port2</th>
                        <th>username2</th>
                        <th>password2</th>
                        <th>dbname2</th>    
                        <th>time_at</th>
                    </tr>
                </thead>
                @foreach( $structure as $key=>$val )
                <tbody>
                    <tr>
                        @foreach( $val as $key1=>$val1 ) 
                            <td>{{ $val1 }}</td>
                        @endforeach
                            <td><a class="btn btn-primary btn-add" onclick="myFunction()">Add form</a></td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        @endif
    </div>
    
    {{$structure->links()}}
        @else
        
        @endif   
    </div>    
    <script>
        $(document).ready(function(){
            $(document).on('click', '.btn-add', function(){
                var tr = $(this).parent().parent();
                $('#hostname').val(tr.find('td:eq(1)').text().trim())
                $('#port').val(tr.find('td:eq(2)').text().trim())
                $('#username').val(tr.find('td:eq(3)').text().trim())
                $('#password').val(tr.find('td:eq(4)').text().trim())
                $('#dbname').val(tr.find('td:eq(5)').text().trim())
                $('#hostname2').val(tr.find('td:eq(6)').text().trim())
                $('#port2').val(tr.find('td:eq(7)').text().trim())
                $('#username2').val(tr.find('td:eq(8)').text().trim())
                $('#password2').val(tr.find('td:eq(9)').text().trim())
                $('#dbname2').val(tr.find('td:eq(10)').text().trim())
            })
        })
    </script>
</body>
</html>
