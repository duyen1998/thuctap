<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    @if( isset($table) )
        @foreach( $table as $key=>$val )
            <h2>{{$key}}</h2>      
            <table class="table">
                <thead>
                <tr>
                    <th>Column</th>
                    <th>Type</th>
                </tr>
                </thead>
                <tbody>
                        @foreach( $val as $key1=>$val1 )
                            <tr>
                                <td>{{ $key1 }}</td>
                                <td>{{ $val1 }}</td>
                            </tr>
                        @endforeach
                    
                
                </tbody>
            </table>
        @endforeach
    @endif
</div>

</body>
</html>
