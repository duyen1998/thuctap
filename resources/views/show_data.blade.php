<!DOCTYPE html>
<html lang="en">

<head>
    <title>Data</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<style>
    

    .h2,
    h2 {
        color: #00A3C7;
        font-weight:bold;
    }

    .groub-form .form-group {
        margin-right: 0 !important;
        margin-left: 0 !important;
    }
    
    .text_err {
        background-color: #E32B39;
        color: #FFFFFF;
    }

    .text_err2 {
        background-color: #E32B39;
        color: #FFFFFF;
    }
    .t_err{
        background-color: #BEE4E7;
    }
    
    .t_err2{
        background-color: #BEE4E7;
    }
    .fa{
        float: right;
        margin-right: 2%;
    }
    .DB-First
    {
      text-align:;
      border:1px solid #000;
    }
    .DBName{
      background:#85CBE9;
      padding: 10px 0 10px 0;
      font-weight:bold;
      margin-top:0;
      margin-left: -0.2px;
    }
    .table-bordered{
        border: 1px solid #080909;
        margin-top: 1%;
        margin-left: -0.5px;
    }
    
    body{
      font-size: 18px;
    }
    .btn{
        font-size: 20px;
    }
    table{
        width:100%;
    }
    table tr td {
        text-align:left;
        padding: 5px;
    }

</style>

<body>
    <form action="{{ url('') }}" method="POST">
        {{ csrf_field()}}
        <div class="jumbotron text-center">
            <h2>SO SÁNH DỮ LIỆU DATABASE</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="checkbox">
                        
                        <div class="DB-First">
                            <h3 class="DBName">Tên DB : {{$db1_name}}</h3>
                            @if( isset($table) )
                            <?php $arr_gt=[
                                'customers'=>'id',
                                'host'=>'host_id',
                                'password'=>'password_id',
                                'port'=>'port_id',
                                'users'=>'id',
                            ];?>
                                @foreach( $table as $key => $val)
                                    <?php 
                                        $arr_property = [];
                                    ?>   
                                    <div class="DataTable table-bordered " style="background:#f2f3fa" >
                                        <h3 <?php echo 'style="color: #E32B39"';?>><?php echo "Table: $key "?></h3>                               
                                    <table class="table_name">
                                        <?php 
                                            foreach( $val as $key1 => $val1 ) {
                                                foreach($val1 as $key_ => $val_){
                                                    array_push($arr_property, $key_);
                                                }
                                                break;
                                            }
                                        ?>      
                                        </thead>
                                            <tr> 
                                                @foreach($arr_property as $val_property)
                                                    <td> {{$val_property}} </td>
                                                @endforeach
                                            </tr>    
                                        </thead>
                                        <tbody>  
                                            @foreach( $val as $key1 => $val1 )
                                            <?php
                                            // dd($val1);
                                                $query = [];
                                                $data = [];
                                                $id = '';
                                                foreach($arr_property as $name_col){
                                                    $data[$name_col] = $val1->$name_col;
                                                    foreach($arr_gt as $val_1){
                                                        if ($val_1 == $name_col){
                                                            $id = $val_1;
                                                        }
                                                    }
                                                }
                                                 array_push($query, $data);
                                                //  dd($query);
                                                    $result = DB::connection('database_02')->table($key)
                                                    ->where($query[0])
                                                    ->first();
                                                    $result_compare = [];
                                                    if($id != ''){
                                                        $result_compare = DB::connection('database_02')->table($key)
                                                        ->where($id, $val1->$id)
                                                        ->get()->toArray();
                                                    }
                                            ?>
                                                <tr <?php 
                                                    if(empty($result_compare)){
                                                        echo 'style="background:#8ad6f7"';
                                                    }
                                                ?>>  
                                                    @foreach($arr_property as $name_col)
                                                        <td
                                                        <?php 
                                                            if(!empty($result_compare)){
                                                                if($result_compare[0]->$name_col != $val1->$name_col)
                                                                echo 'style="background:#E32B39 ;color:#FFFFFF"';
                                                            }
                                                        ?>
                                                        >
                                                            <?php echo $val1->$name_col ;?> 
                                                        </td>
                                                    @endforeach
                                                </tr> 
                                            @endforeach
                                        </tbody>
                                    </table>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>
                </div>
                          
                <div class="col-sm-6">
                    <div class="checkbox">
                        <div class="DB-First">
                            <h3 class="DBName"> <?php echo "Tên DB: $db2_name";?></h3>
                            @if( isset($table2) )
                            <?php $arr_gt2=[
                                'customers'=>'id',
                                'host'=>'host_id',
                                'password'=>'password_id',
                                'port'=>'port_id',
                                'users'=>'id',
                            ];?>
                            @foreach( $table2 as $key=>$val )
                                <?php 
                                    $arr_property2 = [];
                                ?> 
                            <div class="DataTable table-bordered" style="background:#f2f3fa">
                                <h3 <?php echo 'style="color: #E32B39"';?>><?php echo "Table: $key "?></h3> 
                                <table class="table_name">
                                            <?php 
                                                foreach( $val as $key2 => $val2 ) {
                                                    foreach($val2 as $key_ => $val_){
                                                        array_push($arr_property2, $key_);
                                                    }
                                                    break;
                                                }
                                            ?>
                                            </thead>
                                                <tr> 
                                                    @foreach($arr_property2 as $val_property2)
                                                        <td> {{$val_property2}} </td>
                                                    @endforeach
                                                </tr>    
                                            </thead>
                                            <tbody>  
                                                @foreach( $val as $key2 => $val2 )
                                                <?php
                                            // dd($val1);
                                            // $query = ['id'=> '1','name'=> 'nguyen','email'=> 'duyen123@gmail.com','password'=> '123','phone'=> '1234567'];
                                                $query2 = [];
                                                $data2 = [];
                                                $id2 = '';
                                                foreach($arr_property2 as $name_col2){
                                                    $data2[$name_col2] = $val2->$name_col2;
                                                    foreach($arr_gt2 as $val_2){
                                                        if ($val_2 == $name_col2){
                                                            $id2 = $val_2;
                                                        }
                                                    }
                                                }
                                                 array_push($query2, $data2);
                                                //  dd($query);
                                                    $result2 = DB::connection('database_01')->table($key)
                                                    ->where($query2[0])
                                                    ->first();
                                                    $result_compare2 = [];
                                                    if($id2 != ''){
                                                        $result_compare2 = DB::connection('database_01')->table($key)
                                                        ->where($id2, $val2->$id2 )
                                                        ->get()->toArray();
                                                    }
                                            ?>
                                                <tr <?php 
                                                    if(empty($result_compare2)){
                                                        echo 'style="background:#8ad6f7"';
                                                    }
                                                ?>>  
                                                    @foreach($arr_property2 as $name_col2)
                                                    <td 
                                                        <?php 
                                                            if(!empty($result_compare2)){
                                                                if($result_compare2[0]->$name_col2 != $val2->$name_col2)
                                                                echo 'style="background:#E32B39 ;color:#FFFFFF"';
                                                            }
                                                        ?>
                                                        >
                                                            <?php echo $val2->$name_col2 ;?> 
                                                        </td>
                                                        @endforeach
                                                    </tr> 
                                                @endforeach
                                        </tbody> 
                                    </table>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                     
                </div>
                
            </div>
            
    </form>
    </div>
</body>

</html>
