<!DOCTYPE html>
<html lang="en">

<head>
    <title>Structure</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<style>
    {
        background-image: url("/thuctap/resources/views/hinhanh1.jpg");
    }

    .h2,
    h2 {
        color: #00A3C7;
        font-weight:bold;
    }

    .groub-form .form-group {
        margin-right: 0 !important;
        margin-left: 0 !important;
    }

    .table>tbody>tr>td,
    .table>tbody>tr>th,
    .table>tfoot>tr>td,
    .table>tfoot>tr>th,
    .table>thead>tr>td,
    .table>thead>tr>th {
        padding: 7px;
        border-top: 0 !important;
    }

    .text_err {
        background-color: #ef3235;
        color: #FFFFFF;
    }

    .text_err2 {
        background-color: #ef3235;
        color: #FFFFFF;
    }
    .t_err{
        background-color: #8AD6F7;
    }
    
    .t_err2{
        background-color: #8AD6F7;
    }
    .fa{
        float: right;
        margin-right: 2%;
    }
    .DB-First
    {
      text-align:;
      border:1px solid #000;
    }
    .DBName{
      background:#85CBE9;
      padding: 10px 0 10px 0;
      font-weight:bold;
      margin-top:0;
      margin-left: -0.5px;
    }
    .table-bordered{
        border: 1px solid #080909;
        margin-top: 1%;
        margin-left: -0.5px;
    }
    
    body{
      font-size: 18px;
    }
    .btn{
        font-size: 20px;
    }


</style>

<body>
    <form action="{{ url('check') }}" method="POST">
        {{ csrf_field()}}
        <div class="jumbotron text-center">
            <h2>SO SÁNH CẤU TRÚC DATABASE</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="checkbox">
                        <div class="DB-First">
                            <h3 class="DBName">Tên DB : {{$db1_name}}</h3>
                            @if( isset($table) )
                            <?php
                                $count_err = 0;
                                $count_err_table = 0;
                            ?>
                            @foreach( $table as $key=>$val )                           
                            <div class="DataTable table-bordered " style="background:#f2f3fa" >
                            <?php
                            $background='';
                            $color=''; 
                            $tam='';
                                 foreach($key_diff_array_first as $key_diff => $val_diff)
                                 {
                                     if($key == $key_diff)
                                     {
                                         $count_err_table++; 
                                         $background= "#E32B39"; 
                                         $tam=1;  
                                         $color="#FFFFFF";
                                     }  
                                 }      
                            ?>
                                <h3 style="background:{{$background}};color:{{$color}}"> <?php echo "Table: $key"?>@if($tam == 1) <i class="fa fa-times" aria-hidden="true"></i>@endif</h3>                               
                                <div> 
                                </div>                     
                            <table class="table">
                                <tbody>
                                    @foreach( $val as $key1=>$val1 )
                                    <?php
                                      $flag_err = 0;
                                      foreach( $arr_value_err1 as $arr_key_err_1=>$arr_value_err_1 ){
                                        if( $arr_key_err_1 == $key ){ // 2 tên bảng giống nhau
                                          
                                          foreach($arr_value_err_1 as $arr_key_err_1_1=>$arr_value_err_1_1  ){
                                            if( $arr_key_err_1_1 == $key1 ){ 
                                            $flag_err = 1;
                                            $count_err++;
                                    ?>
                                    <tr>
                                        <td class="text_err">{{ $key1 }} </td>
                                        <td class="text_err">{{ $val1 }} <i class="fa fa-times" aria-hidden="true"> </i> </td>
                                    </tr>
                                    <?php }
                                          }
                                        }
                                      }
                                    ?>
                                    <?php 
                                        if( $flag_err == 0 ){
                                        ?>
                                    <tr>
                                        <td class="t_err">{{ $key1 }} </td>
                                        <td class="t_err">{{ $val1 }} <i class="fa fa-check" aria-hidden="true"></i> </td>
                                    </tr>
                                    <?php } ?>
                                    @endforeach
                                </tbody>
                           
                            </table>
                            </div>
                            @endforeach
                            @endif
                            
                        </div>
                    </div>
                    <?php
                        echo "Tổng số trường khác: $count_err";
                        echo "<br>Tổng số bảng khác: $count_err_table";
                    ?> 
                </div>
                          
                <div class="col-sm-6">
                    <div class="checkbox">

                        <div class="DB-First">
                            <h3 class="DBName"> <?php echo "Tên DB: $db2_name";?></h3>
                            @if( isset($table2) )
                            <?php
                                $count_err1 = 0;
                                $count_err_table1 = 0;
                            ?>
                            @foreach( $table2 as $key=>$val )
                            <div class="DataTable table-bordered" style="background:#f2f3fa">
                            <?php
                            $background='';
                            $color=''; 
                            $tam='';
                                 foreach($key_diff_array_second as $key_diff => $val_diff)
                                 {
                                     if($key == $key_diff)
                                     {
                                         $count_err_table1++; 
                                         $background= "#E32B39"; 
                                         $tam=1;  
                                         $color="#FFFFFF";
                                     }  
                                 }      
                            ?>
                                <h3 style="background:{{$background}};color:{{$color}}"> <?php echo "Table: $key"?>@if($tam == 1) <i class="fa fa-times" aria-hidden="true"></i>@endif</h3>
                            
                            <table class="table">
                                <tbody>
                                    @foreach( $val as $key2=>$val2 )
                                    <?php
                                      
                                      $flag_err = 0;
                                      foreach( $arr_value_err2 as $arr_key_err_2=>$arr_value_err_2 ){
                                        if( $arr_key_err_2 == $key ){ // 2 tên bảng giống nhau
                                          foreach($arr_value_err_2 as $arr_key_err_2_1=>$arr_value_err_2_1  ){
                                            if( $arr_key_err_2_1 == $key2 ){ 
                                            $flag_err = 1;
                                            $count_err1++;
                                            ?>
                                    <tr>
                                        <td class="text_err2">{{ $key2 }}</td>
                                        <td class="text_err2">{{ $val2 }}<i class="fa fa-times" aria-hidden="true"></i></td>
                                    </tr>
                                    <?php }
                                          }
                                        }
                                      }
                                    ?>
                                    <?php 
                                        if( $flag_err == 0 ){
                                        ?>
                                    <tr>
                                        <td class="t_err2">{{ $key2 }}</td>
                                        <td class="t_err2">{{ $val2 }} <i class="fa fa-check" aria-hidden="true"></i> </td>
                                    </tr>
                                    <?php } ?>
                                    @endforeach
                                </tbody>
                            </table>
                            </div>
                            @endforeach
                            @endif
                        </div>
                    </div>
                     
                </div>
                <?php
                    echo "Tổng số trường khác: $count_err1";
                    echo "<br>Tổng số bảng khác  : $count_err_table1";
                ?>
            </div>
            <div style="text-align:center">
                <button type="submit" class="btn btn-primary"> Kiểm tra dữ liệu</button>
            </div>
    </form>
   
    </div>
</body>

</html>
