-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th8 19, 2020 lúc 06:43 AM
-- Phiên bản máy phục vụ: 10.4.13-MariaDB
-- Phiên bản PHP: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `test1`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(7, '2014_10_12_000000_create_users_table', 1),
(8, '2014_10_12_100000_create_password_resets_table', 1),
(9, '2019_08_19_000000_create_failed_jobs_table', 1),
(10, '2020_08_12_062842_create_structures_table', 2),
(11, '2020_08_12_063105_create_news_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `structure`
--

CREATE TABLE `structure` (
  `id` int(10) NOT NULL,
  `hostname` varchar(50) NOT NULL,
  `port` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `dbname` varchar(50) NOT NULL,
  `hostname2` varchar(50) NOT NULL,
  `port2` varchar(50) NOT NULL,
  `username2` varchar(50) NOT NULL,
  `password2` varchar(50) NOT NULL,
  `dbname2` varchar(50) NOT NULL,
  `time_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `structure`
--

INSERT INTO `structure` (`id`, `hostname`, `port`, `username`, `password`, `dbname`, `hostname2`, `port2`, `username2`, `password2`, `dbname2`, `time_at`) VALUES
(1, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:34:56'),
(2, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:35:14'),
(3, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:35:23'),
(4, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:35:34'),
(5, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:36:48'),
(6, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:02'),
(7, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:13'),
(8, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:22'),
(9, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:31'),
(10, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:41'),
(11, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:49'),
(12, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 09:37:58'),
(20, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 10:06:25'),
(21, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-17 10:10:42'),
(22, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-18 09:07:40'),
(23, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-18 09:10:28'),
(24, '127.0.0.1', '3306', 'root', '', 'db1', '127.0.0.1', '3306', 'root', '', 'db2', '2020-08-19 03:49:28');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'ea', 'eggre', NULL, 'grfa', NULL, NULL, NULL),
(2, 'gxfhdx', 'dgtdf', NULL, 'xdgd', NULL, NULL, NULL),
(3, 'taylor', 'taylor@example.com', NULL, 'fsfsdg', NULL, NULL, NULL),
(4, 'dayle', 'dayle@example.com', NULL, 'sf', NULL, NULL, NULL),
(5, 'John', 'john@example.com', NULL, 'jfgsjdfgs', NULL, NULL, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `structure`
--
ALTER TABLE `structure`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
