<?php
namespace App\Repositories;
use App\Models\Users;
/**
 ***************************************************************************
 * Repository Assess
 ***************************************************************************
 *
 * This is a repository to query City data
 *
 ***************************************************************************
 * @author: Nhan
 ***************************************************************************
 */
class UsersRepository extends BaseRepository
{
    public function __construct(){
        $this->model = new Users();
    }

}
?>