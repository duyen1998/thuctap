<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    //
    /**
      * The connection name for the model.
      *
      * @var string
      */
      protected $connection = 'test1';
    /**
      * The table associated with the model.
      *
      * @var string
      */
    protected $table = 'structure';
    /**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
    protected $fillable = [
        'id', 'hostname', 'port', 'username', 'password', 'dbname', 'hostname2'. 'port2', 'password2','dbname2',
        
    ];  
}
