<?php
class Migrations extends Eloquent {
    protected $connection = 'migrations';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'migration',
        'batch',
    ];
}