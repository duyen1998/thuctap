<?php
class Failed_jobs extends Eloquent {
    protected $connection = 'failed_jobs';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'connection',
        'queue',
        'payload',
        'exception',
        'failed_at',
    ];
}