<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Repositories\UsersRepository;
use Config;

class UsersController extends Controller
{
    /**
     * Show a list of all of the application's users.
     *
     * @return Response
     */
    public function index()
    {
        $users = DB::table('users')->select('*')->get();

        $users2 = DB::connection('mysql2')->table('users')->select('*')->get();
        return view('index', ['users' => $users,'users2'=>$users2]);
        //DB::table('test1')->get();
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current',$tables);
        dd($tables);
    }

    public function insert(){
        DB::table('users')->insert([
            ['name' => 'taylor','email' => 'taylor@example.com','password' => 'fsfsdg'],
            ['name' => 'dayle','email' => 'dayle@example.com','password' => 'sf']
        ]);    
        DB::connection('mysql2')->table('users')->insert([
            ['name' => 'sam','email' => 'sam@example.com','password' => 'thr'],
            ['name' => 'xamxam','email' => 'xamxam@example.com','password' => 'sfdhg'] 
            
        ]);
          
        // $sortBy = null;

        // $users = DB::table('users')
        //         ->when($sortBy, function ($query, $sortBy) {
        //             return $query->orderBy($sortBy);
        //         }, function ($query) {
        //             return $query->orderBy('name');
        //         })
        //         ->get();
        // return view('user.insert', ['users' => $users]);
    
    }
    public function update(){
        DB::table('users')->updateOrInsert(
            ['email' => 'john@example.com', 'name' => 'John','password' => 'jfgsjdfgs']
        );
        DB::connection('mysql2')->table('users')->updateOrInsert(
            ['email' => 'john@example.com', 'name' => 'John','password' => 'jfgsjdfgs']
        );
    }

    public function view(){
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current',$tables);
        $arr_table = []; 
        foreach( $tables as $tb ){
            $columns = DB::select( DB::raw('SHOW COLUMNS FROM `'.$tb.'`'));
             foreach($columns as $column) {
                $arr_table["$tb"][$column->Field]=$column->Type;
            }
        }
       $data['table'] = $arr_table;
        return view('view', $data);
    }
    public function setcon(){
        Config::set('setima.images', 'mimagick');
        dd(Config::get('setima.images'));
        // $t= config('setima.images');
        // dd($t);
            
    }
   
}
