<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Config;
use Session;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;

class HandleController extends Controller
{
    public function getForm(Request $request){
        $check=0;
        $structure = [];
        $history = $request->get('history');
        if(isset($history) && $history==1){
            $check=1;
            $structure = DB::table('structure')->paginate(10);
            // dd($structure);
            return view('display', compact('structure', 'check'));
        } else{    
            return view('display');
        }
    }


        
    public function handleRequest(Request $request){
        // Nhan het du lieu co trong form
        // dd($request->input());
        $request_db_1 = $request->input('database');
        $request_db_2 = $request->input('database2');
        $database_01 = [];
        $database_02 = [];
        //Mang data1
        
        $database_01['host'] = $request_db_1['hostname'];
        $database_01['port'] = $request_db_1['port'];
        $database_01['dbname'] = $request_db_1['dbname'];
        $database_01['username'] = $request_db_1['username'];
        $database_01['password'] = $request_db_1['password'];
        //dd($database_01);
        //Mang data2
       
        $database_02['host'] = $request_db_2['hostname2'];
        $database_02['port'] = $request_db_2['port2'];
        $database_02['dbname'] = $request_db_2['dbname2'];
        $database_02['username'] = $request_db_2['username2'];
        $database_02['password'] = $request_db_2['password2'];
       //dd($database_02);

       //Gan mang lay CSDL tu data (dataabase_01)
        $db1 = [
            'driver' => 'mysql',         
            'host' => $request_db_1['hostname'],
            'port' =>  $request_db_1['port'],
            'database' => $request_db_1['dbname'],
            'username' => $request_db_1['username'],
            'password' => $request_db_1['password'],
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ];

        Config::set('database.connections.database_01', $db1);
       
        //Gan mang lay CSDL tu data (dataabase_02)
        $db2 = [
            'driver' => 'mysql',
            'host' => $request_db_2['hostname2'],
            'port' =>  $request_db_2['port2'],
            'database' => $request_db_2['dbname2'],
            'username' => $request_db_2['username2'],
            'password' => $request_db_2['password2'],
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ];
        
        Config::set('database.connections.database_02', $db2);

        
        //Kiem tra 2 database co ket noi thanh cong k 
        DB::reconnect("database_01");
        DB::reconnect("database_02");

        $pdo = DB::reconnect('database_01')->getPdo();
        if($pdo){
            echo "Connected successfully to database ".DB::connection('database_01')->getDatabaseName();
        } else {
            echo "You are not connected to database";
        }

        $pdo2 = DB::reconnect('database_02')->getPdo();
        if($pdo2){
            echo "Connected successfully to database ".DB::connection('database_02')->getDatabaseName();
        } else {
            echo "You are not connected to database";
        }

        // Luu 2 gia tri database vao session
        $request->session()->put('db01', $database_01);
        $value = $request->session()->get('db01');
        
        $request->session()->put('db02', $database_02);
        $value2 = $request->session()->get('db02');
        return redirect('compare');
    }

    public function compare(Request $request){
        // dd($request->all());
        // dd($request->input());
        // Nhan het du lieu co trong form
        $request_db_1 = $request->input('database');
        $request_db_2 = $request->input('database2');

        // dd($request_db_1, $request_db_2);
        $database_01 = [];
        $database_02 = [];
        
        //Mang data1
        
        $database_01['host'] = $request_db_1['hostname'];
        $database_01['port'] = $request_db_1['port'];
        $database_01['dbname'] = $request_db_1['dbname'];
        $database_01['username'] = $request_db_1['username'];
        $database_01['password'] = $request_db_1['password'];
        // dd($database_01);
        //Mang data2
       
        $database_02['host'] = $request_db_2['hostname2'];
        $database_02['port'] = $request_db_2['port2'];
        $database_02['dbname'] = $request_db_2['dbname2'];
        $database_02['username'] = $request_db_2['username2'];
        $database_02['password'] = $request_db_2['password2'];
        //insert data vào table
       
        // dd($request_db_2);
        $db1 = [
            'driver' => 'mysql',         
            'host' => $request_db_1['hostname'],
            'port' =>  $request_db_1['port'],
            'database' => $request_db_1['dbname'],
            'username' => $request_db_1['username'],
            'password' => $request_db_1['password'],
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ];
        Config::set('database.connections.database_01', $db1);
        
        //Gan mang lay CSDL tu data (dataabase_02)
        $db2 = [
            'driver' => 'mysql',
            'host' => $request_db_2['hostname2'],
            'port' =>  $request_db_2['port2'],
            'database' => $request_db_2['dbname2'],
            'username' => $request_db_2['username2'],
            'password' => $request_db_2['password2'],
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ];
        Config::set('database.connections.database_02', $db2);

        DB::table('structure')->insert(['hostname' => '127.0.0.1', 'port' => '3306', 'dbname' => 'db1', 'username' => 'root', 'password' => '', 
        'hostname2' => '127.0.0.1', 'port2' => '3306', 'dbname2' => 'db2', 'username2' => 'root', 'password2' => '']);

        //Kiem tra 2 database co ket noi thanh cong k 
        DB::reconnect("database_01");
        DB::reconnect("database_02");

        // $pdo = DB::reconnect('database_01')->getPdo();
        // if($pdo){
        //     echo "Connected successfully to database ".DB::connection('database_01')->getDatabaseName();
        // } else {
        //     echo "You are not connected to database";
        // }

        // $pdo2 = DB::reconnect('database_02')->getPdo();
        // if($pdo2){
        //     echo "Connected successfully to database ".DB::connection('database_02')->getDatabaseName();
        // } else {
        //     echo "You are not connected to database";
        // }

        // Luu 2 gia tri database vao session
        $request->session()->put('db01', $database_01);
        $value = $request->session()->get('db01');
        
        $request->session()->put('db02', $database_02);
        $value2 = $request->session()->get('db02');
        $request_db_1=Session::get("db01");
        $request_db_2=Session::get("db02");

        $data=[];
        $pdo = DB::reconnect('database_01')->getPdo();
        $data_name_table=[];
        if($pdo){   
            //Ket noi voi data1
            $data['db1_name'] = DB::connection('database_01')->getDatabaseName();
            $data['db1_tables'] = DB::connection('database_01')->select('SHOW TABLES');
            $arr_table=[];
            foreach( $data['db1_tables'] as $tb ){//1
                $columns =DB::connection('database_01')->select( DB::raw('SHOW COLUMNS FROM `'.$tb->Tables_in_db1.'`'));
                foreach($columns as $column) {
                    $arr_table["$tb->Tables_in_db1"][$column->Field]=$column->Type;

                }
            }
               
            // dd($data_name_table);
            $data['table'] = $arr_table;       
            // dd($data);
            //Ket noi voi data2
            $data['db2_name']=DB::connection('database_02')->getDatabaseName();
            $data['db2_tables']=DB::connection('database_02')->select('SHOW TABLES');
            $arr_table2=[];
                foreach( $data['db2_tables'] as $tb ){
                $columns2 =DB::connection('database_02')->select( DB::raw('SHOW COLUMNS FROM `'.$tb->Tables_in_db2.'`'));
                foreach($columns2 as $column2) {
                    $arr_table2["$tb->Tables_in_db2"][$column2->Field]=$column2->Type;
                    // foreach(  $arr_table2 as $name_tb2=>$value) {
                    //     $data_in_table2 = DB::connection('database_02')->table($name_tb2)->get();
                    //         // dd($data_in_table2);   
                    // }
                }
                
            }
            $data['table2'] = $arr_table2;
            // $data['name_table2'] = $data_in_table2;
          
        }
        // dd($data);

        foreach($data['table'] as $key_tb1=>$tb1) {
            foreach($data['table2'] as $key_tb2=>$tb2){

                if( $key_tb1 == $key_tb2){
                    //kiểm tra
                    $result_key1 = array_diff_key($tb1,$tb2);  
                    $result_value1 = array_diff_assoc($tb1,$tb2); 
                    $result_key2 = array_diff_key($tb2,$tb1);
                    $result_value2 = array_diff_assoc($tb2,$tb1);
                   
                    // thêm vào mảng lỗi
                    $arr_key_err1[$key_tb1] = $result_key1;
                    $arr_value_err1[$key_tb1] = $result_value1;
                    $arr_key_err2[$key_tb2] = $result_key2;
                    $arr_value_err2[$key_tb2] = $result_value2;
                    // dd($arr_key_err1[$key_tb1], $arr_value_err1[$key_tb1], $arr_key_err2[$key_tb2], $arr_value_err2[$key_tb2]);
                } 
            }
        }
        
        $key_diff_array_first = array_diff_key($data['table'],$data['table2']);
        $key_diff_array_second = array_diff_key($data['table2'],$data['table']);
        // dd($key_diff_array_first,$key_diff_array_second);
        $data['key_diff_array_first'] = $key_diff_array_first;
        $data['key_diff_array_second'] = $key_diff_array_second;
        $data['result_key1'] = $result_key1;
        $data['arr_value_err1'] = $arr_value_err1;
        $data['result_key2'] = $result_key2;
        $data['arr_value_err2'] = $arr_value_err2;   
        // dd($data);
        return view('show', $data);
        // return view('check', $data);
        // return redirect('comparedata');
    }
    
    public function comparedata(Request $request){
        
        $request_db_1=Session::get("db01");
        $request_db_2=Session::get("db02");
        // dd($request_db_2);
        $db1 = [
            'driver' => 'mysql',         
            'host' => $request_db_1['host'],
            'port' =>  $request_db_1['port'],
            'database' => $request_db_1['dbname'],
            'username' => $request_db_1['username'],
            'password' => $request_db_1['password'],
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ];
        Config::set('database.connections.database_01', $db1);

        //Gan mang lay CSDL tu data (dataabase_02)
        $db2 = [
            'driver' => 'mysql',
            'host' => $request_db_2['host'],
            'port' =>  $request_db_2['port'],
            'database' => $request_db_2['dbname'],
            'username' => $request_db_2['username'],
            'password' => $request_db_2['password'],
            'unix_socket' => '',
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'strict' => true,
            'engine' => null,
            'options'   => [
                \PDO::ATTR_EMULATE_PREPARES => true
            ]
        ];
        Config::set('database.connections.database_02', $db2);

        $datac=[];
        $pdo = DB::reconnect('database_01')->getPdo();
        $data_name_table=[];
        if($pdo){   
            //Ket noi voi data1
            $datac['db1_name'] = DB::connection('database_01')->getDatabaseName();
            $datac['db1_tables'] = DB::connection('database_01')->select('SHOW TABLES'); 
            $arr_table=[];
            foreach( $datac['db1_tables'] as $tb ){//1
                $columns =DB::connection('database_01')->select( DB::raw('SHOW COLUMNS FROM `'.$tb->Tables_in_db1.'`'));
                foreach($columns as $column) {
                    $arr_table["$tb->Tables_in_db1"][$column->Field]=$column->Type;
                }
            }
            foreach( $arr_table as $arr_tables=>$value) {
                $data_in_table = DB::connection('database_01')->table($arr_tables)->get()->toArray();
                array_push($data_name_table,$data_in_table);
            } 
            $count = 0;
            $arr_temp = [];
            $data_tab=$request->get('db');
            foreach($arr_table as $key => $val){
                if(count($data_name_table[$count]) > 0){//Neu table empty thi push thuoc tinh mac dinh vao mang [table_name]
                        foreach($data_tab as $key_table ) {
                            if($key==$key_table){
                                $arr_temp[$key] = $data_name_table[$count];
                            }
                        }
                }
                $count++;
            }
            $datac['table'] = $arr_temp;
            
            // dd($arr_temp);
            //Ket noi voi data2
            $data_name_table2=[];
            $datac['db2_name']=DB::connection('database_02')->getDatabaseName();
            $datac['db2_tables']=DB::connection('database_02')->select('SHOW TABLES');
            $arr_table2=[];
                foreach( $datac['db2_tables'] as $tb ){
                $columns2 =DB::connection('database_02')->select( DB::raw('SHOW COLUMNS FROM `'.$tb->Tables_in_db2.'`'));
                foreach($columns2 as $column2) {
                    $arr_table2["$tb->Tables_in_db2"][$column2->Field]=$column2->Type;
                    
                }
            }
                foreach( $arr_table2 as $arr_tables2=>$value2) {
                    $data_in_table2= DB::connection('database_02')->table($arr_tables2)->get();
                    array_push($data_name_table2,$data_in_table2);
                }   
            
            // dd($data_name_table2); 
            $count2 = 0;
            $arr_temp2 = [];
            foreach($arr_table2 as $key => $val){
                if(count($data_name_table2[$count2]) > 0){//Neu table2 empty thi push thuoc tinh mac dinh vao mang [table_name2]
                    foreach($data_tab as $key_table) {
                        if($key==$key_table){
                            $arr_temp2[$key] = $data_name_table2[$count2];                                
                        }
                    }
                }
                $count2++;
            }
            $datac['table2'] = $arr_temp2;
        }
        

        return view('show_data', $datac);
    }

        
}